$(document).ready(function() {
  $("body").prognroll({
    height: 3,
    color: "#FF9934",
    custom: false
  });

  $(".filter__button").click(function() {
    $(".card").each(function() {
      var ths = $(this);
      var card_sum = parseInt(ths.attr("data-sum"));
      var required_sum = parseInt($("#sum_input").val());

      if (card_sum < required_sum) {
        ths.hide();
      } else {
        ths.show();
      }
    });
  });

  function sum_slider_change() {
    var x = $("#sum_slider").val();
    console.log(x);
    $("#sum_input").val(x);
    var color =
      "linear-gradient(90deg, #FF9934 " +
      x / 1000 +
      "%, #E8E8E8 " +
      x / 1000 +
      "%)";
      $("#sum_slider").css("background", color);
  }

  function sum_input_change() {
    var val = $("#sum_input").val();
    $("#sum_slider").val(val);
    sum_slider_change();
  }

  $("#sum_input").on("input", sum_input_change);
  $("#sum_slider").on("mousemove", sum_slider_change);
});


// var sum_slider = document.getElementById("sum_slider"),
//   term_slider = document.getElementById("term_slider");

// var sum_input = document.getElementById("sum_input"),
//   term_input = document.getElementById("term_input");

// // TODO: complete it
// function term_slider_change() {
//   var x = term_slider.value;
//   term_input.value = x;
//   var color = "linear-gradient(90deg, #FF9934 " + x + "%, #E8E8E8 " + x + "%)";
//   term_slider.style.background = color;
// }

// function sum_input_change() {
//   var val = sum_input.value;
//   console.log(val);
//   sum_slider.value = val;
//   sum_slider_change();
// }

// function term_input_change() {
//   var val = term_input.value;
//   term_slider.value = val;
//   term_slider_change();
// }

// sum_input.addEventListener("input", sum_input_change);
// term_input.addEventListener("input", term_input_change);

// sum_slider.addEventListener("touchmove", sum_slider_change);
// sum_slider.addEventListener("mousemove", sum_slider_change);
// sum_slider.addEventListener("click", sum_slider_change);

// term_slider.addEventListener("touchmove", term_slider_change);
// term_slider.addEventListener("mousemove", term_slider_change);
// term_slider.addEventListener("click", term_slider_change);
